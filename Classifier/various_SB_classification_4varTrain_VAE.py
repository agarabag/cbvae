import pandas as pd
from scipy.stats import wasserstein_distance
#import keras
import tensorflow as tf
import numpy as np
from tensorflow import keras
from tensorflow.keras.layers import Dense, Dropout, Input, BatchNormalization, Concatenate
from tensorflow.keras.models import Model,Sequential
from tensorflow.keras.datasets import mnist
from tqdm import tqdm
#from tensorflow.keras.layers.advanced_activations import LeakyReLU
from tensorflow.keras.optimizers import Adam
from tensorflow.keras import callbacks
from sklearn.metrics import roc_auc_score, roc_curve
from sklearn.model_selection import train_test_split

from sklearn.utils import class_weight, shuffle
import matplotlib.pyplot as plt

from sklearn.preprocessing import StandardScaler


nFeat = 4
n_gen = 400000

SB_ratios = [0.6, 0.4, 0.3, 0.2, 0.15, 0.1, 0.05]
seed_num = [1,2,3,4,5,6,7,8,9,10]
# SB_ratios = [0.1, 0.15, 0.05]



for ratio in SB_ratios:
    for seed in seed_num:

        tf.keras.backend.clear_session()
        
        # load real mc sample
        innerdata_train = np.load("/global/homes/a/agarabag/mc_6var_SB_s/mc_6var_SB"+str(ratio)+"_s"+str(seed)+"/innerdata_train_6var.npy")
        X_mc_train = innerdata_train[:,1:nFeat+1]
        print("shape of X_mc_train: ", X_mc_train.shape)
        innerdata_val = np.load("/global/homes/a/agarabag/mc_6var_SB_s/mc_6var_SB"+str(ratio)+"_s"+str(seed)+"/innerdata_val_6var.npy")
        X_mc_val = innerdata_val[:,1:nFeat+1]
        print("shape of X_mc_val: ", X_mc_val.shape)

        # load generated bkg sample
        fileNameNN = '/global/homes/a/agarabag/cbvae/pre_trained_data/cBVAE-6var/gen_data/LHCO2020_cB-VAE_events_lr1en3_NoF_SR.csv'
        gen_SR = pd.read_csv(fileNameNN, delimiter = ' ', header=None, index_col=False)
        gen_SR = gen_SR.to_numpy()
        gen_SR = gen_SR[:,:nFeat]
        print("shape of gen_SR: ", gen_SR.shape)
        gen_SR_train = gen_SR[:int(n_gen/2)]
        print("shape of gen_SR_train", gen_SR_train.shape)
        gen_SR_val = gen_SR[int(n_gen/2):]
        print("shape of gen_SR_val", gen_SR_val.shape)

        # load test mc sample
        innerdata_test = np.load("/global/homes/a/agarabag/mc_6var_SB_s/mc_6var_SB"+str(ratio)+"_s"+str(seed)+"/innerdata_test_6var.npy")
        innerdata_extrabkg_test = np.load("/global/homes/a/agarabag/mc_6var_SB_s/mc_6var_SB"+str(ratio)+"_s"+str(seed)+"/innerdata_extrabkg_test_6var.npy")
        innerdata_test = np.concatenate((innerdata_test, innerdata_extrabkg_test), axis=0)
        X_mc_test = innerdata_test[:,1:nFeat+1]
        print("shape of X_mc_test: ", X_mc_test.shape)

        X_train_mc_and_gen = np.concatenate((X_mc_train, gen_SR_train), axis=0)
        print("shape of training mc and data combination: ", X_train_mc_and_gen.shape)

        X_val_mc_and_gen = np.concatenate((X_mc_val, gen_SR_val), axis=0)
        print("shape of val mc and data combination: ", X_val_mc_and_gen.shape)

        y_train_mc = np.ones(X_mc_train.shape[0])
        print("shape of train_mc label: ", y_train_mc.shape)

        y_train_gen = np.zeros(gen_SR_train.shape[0])
        print("shape of train_gen label: ", y_train_gen.shape)

        y_val_mc = np.ones(X_mc_val.shape[0])
        print("shape of val_mc label: ", y_val_mc.shape)

        y_val_gen = np.zeros(gen_SR_val.shape[0])
        print("shape of val_gen label: ", y_val_gen.shape)

        y_mc_test = innerdata_test[:,nFeat+3]
        print("shape of mc_test label: ", y_mc_test.shape)

        y_train_mc_and_gen = np.concatenate((y_train_mc, y_train_gen), axis=0)
        print("shape of training mc and data label combination: ", y_train_mc_and_gen.shape)

        y_val_mc_and_gen = np.concatenate((y_val_mc, y_val_gen), axis=0)
        print("shape of val mc and data label combination: ", y_val_mc_and_gen.shape)

        shuffle_seed = 42
        X_train_mc_and_gen, y_train_mc_and_gen = shuffle(X_train_mc_and_gen, y_train_mc_and_gen, random_state = shuffle_seed)
        X_val_mc_and_gen, y_val_mc_and_gen = shuffle(X_val_mc_and_gen, y_val_mc_and_gen, random_state = shuffle_seed)
        X_mc_test, y_mc_test = shuffle(X_mc_test, y_mc_test, random_state = shuffle_seed)

        scaler = StandardScaler()
        scaler.fit(X_train_mc_and_gen)
        #transform training dataset
        X_train_mc_and_gen = scaler.transform(X_train_mc_and_gen)
        # transform val dataset
        X_val_mc_and_gen = scaler.transform(X_val_mc_and_gen)

        # transform test dataset
        X_mc_test = scaler.transform(X_mc_test)


        # es = callbacks.EarlyStopping(monitor='loss',min_delta = 1e-3, mode='min', verbose=1, patience=5)
        optimizer = Adam(lr=1e-3)
        model = Sequential()
        model.add(Dense(64, input_dim=nFeat, activation='relu'))
        model.add(Dense(64, activation='relu'))
        model.add(Dense(64, activation='relu'))
        model.add(Dense(1, activation='sigmoid'))
        model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])


        history = tf.keras.callbacks.History()
        num_epoch=50
        batch_size=128
        class_weights = class_weight.compute_class_weight('balanced',np.unique(y_train_mc_and_gen),y_train_mc_and_gen)
        class_weights_dict = dict(enumerate(class_weights))

        class CustomSaver(callbacks.Callback):
            def on_epoch_end(self, epoch, logs={}):
                    self.model.save("/global/homes/a/agarabag/classifier_cBVAE_6varInput_4varTrain/model_cBVAE_4var_SB"
                                    +str(ratio)+"_s"+str(seed)+"_epoch{}.h5".format(epoch))

        saver = CustomSaver()
        
        # config = tf.compat.v1.ConfigProto()
        # config.gpu_options.per_process_gpu_memory_fraction = 0.9
        # config.gpu_options.allow_growth = True
        # config.gpu_options.polling_inactive_delay_msecs = 10
        # session = tf.compat.v1.Session(config=config)

        model.fit(X_train_mc_and_gen, y_train_mc_and_gen, 
                epochs=num_epoch,
                batch_size=batch_size,
                class_weight=class_weights_dict,
                validation_data=(X_val_mc_and_gen, y_val_mc_and_gen), 
                shuffle=shuffle,
                verbose=1,callbacks=[history,saver])

        num_models = 10
        idx_best_models = np.argpartition(history.history['val_loss'], num_models)[:num_models]
        print(idx_best_models)

        Train_Predictions = []
        Val_Predictions = []
        Test_Predictions = []
        for idx in idx_best_models:
            model_temp = keras.models.load_model("/global/homes/a/agarabag/classifier_cBVAE_6varInput_4varTrain/model_cBVAE_4var_SB"+str(ratio)+"_s"+str(seed)+"_epoch"+str(idx)+".h5")
            Train_Predictions.append(model_temp.predict(X_train_mc_and_gen))
            Val_Predictions.append(model_temp.predict(X_val_mc_and_gen))
            Test_Predictions.append(model_temp.predict(X_mc_test))


        predict_test = np.mean(Test_Predictions, axis=0)
        np.save("/global/homes/a/agarabag/predictions/predict_test_cBVAE_SB"+str(ratio)+"_s"+str(seed)+"_4varTrain.npy", predict_test)
        print(predict_test.shape)



# for ratio in SB_ratios:
#     # load real mc sample
#     innerdata_train = np.load("/global/u2/a/agarabag/anomoly_studies/mc_generation/mc_sb"+str(ratio)+"_data/innerdata_train.npy")
#     X_mc_train = innerdata_train[:,1:nFeat+1]
#     print("shape of X_mc_train: ", X_mc_train.shape)
#     innerdata_val = np.load("/global/u2/a/agarabag/anomoly_studies/mc_generation/mc_sb"+str(ratio)+"_data/innerdata_val.npy")
#     X_mc_val = innerdata_val[:,1:nFeat+1]
#     print("shape of X_mc_val: ", X_mc_val.shape)

#     # load generated bkg sample
#     fileNameNN = '/global/homes/a/agarabag/LHCO2020-cVAE/pre_trained_data/cBVAE-6var/gen_data/LHCO2020_cB-VAE_events_lr1en3_NoF_SR.csv'
#     gen_SR = pd.read_csv(fileNameNN, delimiter = ' ', header=None, index_col=False)
#     gen_SR = gen_SR.to_numpy()
#     gen_SR = gen_SR[:,:nFeat]
#     print("shape of gen_SR: ", gen_SR.shape)
#     gen_SR_train = gen_SR[:int(n_gen/2)]
#     print("shape of gen_SR_train", gen_SR_train.shape)
#     gen_SR_val = gen_SR[int(n_gen/2):]
#     print("shape of gen_SR_val", gen_SR_val.shape)

#     # load test mc sample
#     innerdata_test = np.load("/global/u2/a/agarabag/anomoly_studies/mc_generation/mc_sb"+str(ratio)+"_data/innerdata_test.npy")
#     innerdata_extrabkg_test = np.load("/global/u2/a/agarabag/anomoly_studies/mc_generation/mc_sb"+str(ratio)+"_data/innerdata_extrabkg_test.npy")
#     innerdata_test = np.concatenate((innerdata_test, innerdata_extrabkg_test), axis=0)
#     X_mc_test = innerdata_test[:,1:nFeat+1]
#     print("shape of X_mc_test: ", X_mc_test.shape)

#     X_train_mc_and_gen = np.concatenate((X_mc_train, gen_SR_train), axis=0)
#     print("shape of training mc and data combination: ", X_train_mc_and_gen.shape)

#     X_val_mc_and_gen = np.concatenate((X_mc_val, gen_SR_val), axis=0)
#     print("shape of val mc and data combination: ", X_val_mc_and_gen.shape)

#     y_train_mc = np.ones(X_mc_train.shape[0])
#     print("shape of train_mc label: ", y_train_mc.shape)

#     y_train_gen = np.zeros(gen_SR_train.shape[0])
#     print("shape of train_gen label: ", y_train_gen.shape)

#     y_val_mc = np.ones(X_mc_val.shape[0])
#     print("shape of val_mc label: ", y_val_mc.shape)

#     y_val_gen = np.zeros(gen_SR_val.shape[0])
#     print("shape of val_gen label: ", y_val_gen.shape)

#     y_mc_test = innerdata_test[:,nFeat+1]
#     print("shape of mc_test label: ", y_mc_test.shape)

#     y_train_mc_and_gen = np.concatenate((y_train_mc, y_train_gen), axis=0)
#     print("shape of training mc and data label combination: ", y_train_mc_and_gen.shape)

#     y_val_mc_and_gen = np.concatenate((y_val_mc, y_val_gen), axis=0)
#     print("shape of val mc and data label combination: ", y_val_mc_and_gen.shape)

#     shuffle_seed = 42
#     X_train_mc_and_gen, y_train_mc_and_gen = shuffle(X_train_mc_and_gen, y_train_mc_and_gen, random_state = shuffle_seed)
#     X_val_mc_and_gen, y_val_mc_and_gen = shuffle(X_val_mc_and_gen, y_val_mc_and_gen, random_state = shuffle_seed)
#     X_mc_test, y_mc_test = shuffle(X_mc_test, y_mc_test, random_state = shuffle_seed)

#     scaler = StandardScaler()
#     scaler.fit(X_train_mc_and_gen)
#     #transform training dataset
#     X_train_mc_and_gen = scaler.transform(X_train_mc_and_gen)
#     # transform val dataset
#     X_val_mc_and_gen = scaler.transform(X_val_mc_and_gen)

#     # transform test dataset
#     X_mc_test = scaler.transform(X_mc_test)


#     # es = callbacks.EarlyStopping(monitor='loss',min_delta = 1e-3, mode='min', verbose=1, patience=5)
#     optimizer = Adam(lr=1e-3)
#     model = Sequential()
#     model.add(Dense(64, input_dim=nFeat, activation='relu'))
#     model.add(Dense(64, activation='relu'))
#     model.add(Dense(64, activation='relu'))
#     model.add(Dense(1, activation='sigmoid'))
#     model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])


#     history = tf.keras.callbacks.History()
#     num_epoch=50
#     batch_size=128
#     class_weights = class_weight.compute_class_weight('balanced',np.unique(y_train_mc_and_gen),y_train_mc_and_gen)
#     class_weights_dict = dict(enumerate(class_weights))

#     class CustomSaver(callbacks.Callback):
#         def on_epoch_end(self, epoch, logs={}):
#             self.model.save("/global/u2/a/agarabag/LHCO2020-cVAE/classifier_CATHODE_6varInput_4varTrain/model_cbvae_4var_SB"+str(ratio)+"_epoch{}.h5".format(epoch))

#     saver = CustomSaver()

#     model.fit(X_train_mc_and_gen, y_train_mc_and_gen, 
#             epochs=num_epoch,
#             batch_size=batch_size,
#             class_weight=class_weights_dict,
#             validation_data=(X_val_mc_and_gen, y_val_mc_and_gen), 
#             shuffle=shuffle,
#             verbose=1,callbacks=[history,saver])

#     num_models = 10
#     idx_best_models = np.argpartition(history.history['val_loss'], num_models)[:num_models]
#     print(idx_best_models)

#     Train_Predictions = []
#     Val_Predictions = []
#     Test_Predictions = []
#     for idx in idx_best_models:
#         model_temp = keras.models.load_model("/global/u2/a/agarabag/LHCO2020-cVAE/classifier_CATHODE_6varInput_4varTrain/model_cbvae_4var_SB"+str(ratio)+"_epoch"+str(idx)+".h5")
#         Train_Predictions.append(model_temp.predict(X_train_mc_and_gen))
#         Val_Predictions.append(model_temp.predict(X_val_mc_and_gen))
#         Test_Predictions.append(model_temp.predict(X_mc_test))


#     predict_test = np.mean(Test_Predictions, axis=0)
#     np.save("/global/u2/a/agarabag/LHCO2020-cVAE/predictions/predict_test_cbvae_SB"+str(ratio)+"_4varTrain.npy", predict_test)
#     print(predict_test.shape)





