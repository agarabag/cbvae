import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from scipy.special import logit, expit

# scaler = StandardScaler()



minmax=np.load("testing_out/testing_data_minmax.npy")
logit=np.load("testing_out/testing_data_minmax_logit.npy")
sclar=np.load("testing_out/testing_data_minmax_logit_scltrans.npy")
raw=np.load("testing_out/testing_data_raw.npy")

# minmax_rev=np.load("testing_out/testing_data_exp_max_rev.npy")
# logit_rev=np.load("testing_out/testing_data_exp_rev.npy")
# sclar_rev=np.load("testing_out/testing_data_scalar_rev.npy")

cond_minmax=np.load("testing_out/testing_cond_data_minmax.npy")
#cond_logit=np.load("testing_out/testing_cond_data_minmax_logit.npy")
cond_sclar=np.load("testing_out/testing_cond_data_minmax_logit_scltrans.npy")
cond_raw=np.load("testing_out/testing_cond_data_raw.npy")

gen_minmax=np.load("testing_out/genereated_scalar_exp_max.npy")
gen_logit=np.load("testing_out/genereated_exp.npy")
gen_sclar=np.load("testing_out/genereated_scalar.npy")
gen_raw=np.load("testing_out/genereated_raw.npy")

SB_gen_minmax=np.load("testing_out/SB_genereated_scalar_exp_max.npy")
SB_gen_logit=np.load("testing_out/SB_genereated_scalar_exp.npy")
SB_gen_sclar=np.load("testing_out/SB_genereated_scalar.npy")
SB_gen_raw=np.load("testing_out/SB_genereated_raw.npy")

# input_cond=np.load("inner_train_cond.npy")

mjj_minmax=np.load("testing_out/mjj_gen_max.npy")
# mjj_logit=np.load("testing_out/mjj_gen_max_logit.npy")
mjj_sclar=np.load("testing_out/mjj_gen_max_logit_scaled.npy")
mjj_raw=np.load("testing_out/mjj_gen_pre_new_transf.npy")

# inner_raw=np.load("testing_out/inner_train_cond_raw.npy")
# inner_transf=np.load("testing_out/inner_train_cond.npy")

# plt.hist(encoder_cond,bins=100, histtype='step', color="red")
# plt.hist(gen_cond,bins=100, histtype='step', color="black")

plt.hist(cond_raw,bins=100, histtype='step', color="green", density=True)
# plt.hist(mjj_sclar,bins=100, histtype='step', color="red", density=True)
# plt.hist(mjj_raw,bins=100, histtype='step', color="orange", density=True)
# plt.hist(raw,bins=100, histtype='step', color="black")

# plt.hist(minmax[:,0],bins=100, histtype='step', color="green", density=True)
# plt.hist(raw[:,0],bins=100, histtype='step', color="blue", density=True)
# plt.hist(gen_minmax[:,0],bins=100, histtype='step', color="red", density=True)
# plt.hist(cond_minmax,bins=100, histtype='step', color="orange", density=True)
# plt.hist(gen_sclar[:,0],bins=100, histtype='step', color="black", density=True)

# plt.hist(sclar[:,0],bins=100, histtype='step', color="blue", density=True)
# x=sclar[:,0]
# x=x[~np.isnan(x)]
# print(x)
# plt.hist(x,bins=100, histtype='step', color="blue", density=True)
# print("mean:", np.mean(x))
# print(np.std(x))
# plt.hist(gen_raw[:,0],bins=100, histtype='step', color="red", density=True)


# plt.hist(logit_rev[:,0],bins=100, histtype='step', color="red", linestyle=(0, (1, 1)))
# plt.hist(sclar_rev[:,0],bins=100, histtype='step', color="black", linestyle=(0, (1, 1)))
# plt.hist(minmax_rev[:,0],bins=100, histtype='step', color="orange", linestyle=(0, (1, 1)))
plt.yscale('log')

plt.savefig('hist.png')
# plt.hist(minmax[:,0],bins=100, histtype='step')
# plt.hist(logit[:,0],bins=100, histtype='step')
# plt.hist(sclar[:,0],bins=100, histtype='step')
# plt.savefig('hist.png')


# plt.hist(minmax[:,0],bins=100, histtype='step')
# print("SHAPE: ", logit.shape)
# # print(np.where(np.isinf(logit[:,0])))
# # print(logit)
# # for i in range()
# logit = logit - 1.0e-2
# # print(logit)

# # logit_good = logit[np.where(~np.isinf(logit[:,0]))]
# plt.hist(logit[:,0],bins=100, histtype='step')
# print(logit[:,0].shape)
# plt.hist(scaler.fit_transform(np.reshape(logit[:,0], [-1,1])),bins=100, histtype='step')
# plt.show()